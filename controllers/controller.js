var bodyParser = require('body-parser');
var mongoose = require ('mongoose');
mongoose.Promise=global.Promise;
mongoose.connect('mongodb://Amogh:webproject1@ds029911.mlab.com:29911/student_parent_faculty', { useNewUrlParser: true });

var schema = new mongoose.Schema({
  firstname : String,
  lastname : String,
  studentemail : String,
  usn : String,
  sem : Number,
  Subject :String,
  IA1 : Number,
  perAttIA1 : Number,
  IA2 : Number,
  perAttIA2 : Number,
  IA3 : Number,
  perAttIA3 : Number,
  AVG : Number,
  perAtt : { type : Number },
  password : String
});
var StudentData = new mongoose.Schema({
  usn : String,
  sem : Number,
  Subject : Array,
  IA1 : Array,
  perAttIA1 : Array,
  IA2 : Array,
  perAttIA2 : Array,
  IA3 : Array,
  perAttIA3 : Array,
  AVG : Array,
  perAtt : { type : Array }
})
var parentSchema= new mongoose.Schema({
  firstname : String,
  lastname : String,
  parentemail : String,
  wardUsn :  String,
    password : String,
});

var facultySchema = new mongoose.Schema({
  firstname : String,
  lastname : String,
  facultyemail : String,
  empid : String,
  dept : String,
  sem : Array,
  sub : Array,
  password : String
});
var dataSchema =  new mongoose.Schema({
  sub : String,
  sem : Number,
  usn : String,
  name : String,
  IA1 : Number,
  perAttIA1 : Number,
  IA2 : Number,
  perAttIA2 : Number,
  IA3 : Number,
  perAttIA3 : Number,
  average : Number,
  perAtt : Number,
});
var facultyDataSchema = new mongoose.Schema({
  id : String,
  sem6sub : String,
  sem7sub : String
});
var trail = new mongoose.Schema({
  usn : String,
  sub : String,
  marks : Number
})

var doubtSchema = new mongoose.Schema({
  usn : String,
  sem: Number,
  sub : String,
  facultyName : String,
  query : String,
  img : { type : String,
          data : Buffer
        }
});
 var trySchema = new mongoose.Schema({
   empid: String,
   usn : Array,
   sem : Number,
   Subject : Array,
   IA1 : Array,
   perAttIA1 : Array,
   IA2 : Array,
   perAttIA2 : Array,
   IA3: Array,
   perAttIA3 :Array,
   AVG : Array,
   perAtt : { type : Array },
 });
 var Trail = mongoose.model('Trail',trail);
 var StudentDataSchema = mongoose.model('StudentDataSchema',StudentData);
 var Try = mongoose.model('Try',trySchema);
var Doubt = mongoose.model('Doubt', doubtSchema);
var facultyData = mongoose.model('facultyData',facultyDataSchema);
var Student = mongoose.model('Student',schema);
var Faculty = mongoose.model('Faculty',facultySchema);
var Parent= mongoose.model('Parent',parentSchema);
var Data = mongoose.model('Data', dataSchema);
/*var item1= Data({usn:'is9090',name : 'A Rao',IA1 : "", perAttIA1: "", perAttIA1: "",IA2 :"",perAttIA2 :"" ,IA3 :"",perAttIA3 : "", AVG :"", percAtt: ""}).save(function(error){
  if(error) throw error;
  console.log('item saved');
});*/
var urlencodedParser = bodyParser.urlencoded({ extended: false });

module.exports = function (app){

  app.get('/doubts',function(req,res){

    Student.find({},function(err,user){
      if(err) throw err;
      Faculty.find({},function(err,fac){
        if(err) throw err;
          res.render('doubts',{student :user, faculty : fac})
      })

    })
    ;
  });
  app.get('/studentRegister',function(req,res){
    res.render('studentReg');
  })
  app.post('/studentRegister', urlencodedParser, function(req,res){
      console.log(req.body.firstname);
      if(Student.find({email:req.body.studentemail, usn:req.body.usn})==[]) {
        //res.end('<p><a href="/views/student_login">Click here!</a></p>\n');
        res.redirect('/alreadyexists');
      }
      else{
        if(req.body.sem == 7){
          req.body.Subject=["SA","WEB","INS","SAN","ML"];

        }
        if(req.body.sem == 6){
          req.body.Subject=["ST","FS","OR","Crypto","OS","Python"];

        }
        console.log(req.body);
        var newData = StudentDataSchema(req.body).save(function(err,data){
          if(err) throw err;

        });

      var newStudent = Student(req.body).save(function(err,data){
      if (err) throw err;
      console.log(data);
      //res.redirect('/student_login');
    });

  }
  });

  app.get('/alreadyexists',function(req,res){
    res.render('alreadyexists');
  });

  app.get('/student_login',function(req,res){
    res.render('student_login');
  });
  app.post('/student_login', urlencodedParser, function(req,res){
    Student.findOne({usn:req.body.usn, password:req.body.password}, function(err,user){
      if(err){
        throw err;
      }
      if (user){
        console.log(user);
        res.redirect('/student1/?usn='+req.body.usn+'&name='+user.firstname);
      }
      else {
        console.log('doesntexisst');
      }
    })
    });
app.get('/student1',function(req,res){
  StudentDataSchema.find({},function(err,user){
    if(err) throw err;
    console.log(user);

      res.render('student1',{ Student : user, usn: req.query.usn, name : req.query.name});
  });

})
  app.get('/parentRegister',function(req,res){
    res.render('parentreg')
  });
  app.post('/parentRegister', urlencodedParser, function(req,res){

    Parent.find({wardUsn: req.body.usn, parentemail : req.body.parentemail}, function(err,user){
     if(err) throw err;
     console.log(user);
      if(user==[])
      {
        res.redirect('/parentalreadyexists');
      }
      else{
        var newParent = Parent(req.body).save(function(err,data){
          if(err) throw err;

        //  res.render('',{usn:req.})
        });
      }
    });
});

app.get('/parentalreadyexists',function(req,res){
  res.render('parentalreadyexists')
});

  //});
  app.get('/parentLogin',function(req,res){
    res.render('parentLogin');
  });
  app.post('/parentLogin',urlencodedParser,function(req,res){
    Parent.findOne({wardUsn:req.body.usn, password:req.body.password}, function(err,user){
      if(err){
        throw err;
      }
      if (user){
        res.render('parent1',{user : user.body});
        console.log(user);
      }
      else {
        console.log('doesntexisst');
        res.render('invalidParent')
      }
    })
  });
  app.get('/parent1',function(req,res){
    res.render('parent1');
    console.log(req.query.var);
  })
  app.get('/facultyRegister', function(req,res){
    res.render('facultyRegister');
  });
  app.post('/facultyRegister', urlencodedParser, function(req,res){
    /*var newFaculty = Faculty(req.body).save(function(err,data){
      if(err) throw err;*/
      Faculty.find({empid: req.body.empid, facultyemail : req.body.facultyemail}, function(err,user){
       if(err) throw err;
        if(user==[])
        {
          res.redirect('/parentalreadyexists');
        }
        else{
          var newFaculty = Faculty(req.body).save(function(err,data){
            if(err) throw err;
            res.redirect('/faculty0/?empid='+req.body.empid)
          });
        }
      });
  });
app.get('/facultyLogin',function(req,res){
  res.render('facultyLogin');
});

app.get('/faculty01/facultyLogin',function(req,res){
  res.redirect('/facultyLogin');
})
app.post('/facultyLogin',urlencodedParser,function(req,res){
  Faculty.findOne({empid :req.body.empid, password:req.body.password}, function(err,user){
    if(err){
      throw err;
    }
    if (user){
        console.log(user.empid);
        res.redirect('/faculty1/?empid='+ user.empid);
      }
    else {
      console.log('doesntexist');
    }
  })
});
app.get('/faculty1',function(req,res){
  //res.render('faculty1',{empid : req.query.data.id});

  StudentDataSchema.find({},function(err,data){
    if(err) throw err;

    var temp = req.query.empid;
    Faculty.findOne({empid : temp},function(err,info){
      res.render('faculty1',{id : info.empid, stu: data,  par :info});
    });

  });
});



app.post('/faculty1', urlencodedParser, function(req,res){
  var index_of_subject=0
  StudentDataSchema.findOne({usn:req.body.usn},(err,student)=>{
    if(err)
      throw err;
    if(student){
      console.log(student)
      index_of_subject=student.Subject.indexOf(req.body.subject);
      student.IA1[index_of_subject]=parseInt(req.body.IA1);
      student.perAttIA1[index_of_subject]=parseInt(req.body.perAttIA1);
      student.IA2[index_of_subject]=parseInt(req.body.IA2);
      student.perAttIA2[index_of_subject]=parseInt(req.body.perAttIA2);
      student.IA3[index_of_subject]=parseInt(req.body.IA3);
      student.perAttIA3[index_of_subject]=parseInt(req.body.perAttIA3);
      student.AVG[index_of_subject]=parseInt(req.body.Avg);
      student.perAtt[index_of_subject]=parseInt(req.body.perAtt);
      StudentDataSchema.findOneAndUpdate({usn:student.usn},{
        IA1: student.IA1, perAttIA1:student.perAttIA1, IA2:student.IA2, perAttIA2:student.perAttIA2, IA3:student.perAttIA3,
        perAttIA3: student.perAttIA3,AVG:student.AVG, perAtt:student.perAtt
      },(err,student1)=>{
        if(err)
          throw err;
        else{
          console.log(student1)
        res.redirect('/faculty1/?empid='+ req.body.empid);
        }
      })

    }
  })
});

app.get('/facultySucessfullUpdatedStudent',function(req,res){
  res.render('facultySucessfullUpdatedStudent',{empid : req.query.empid})
});
app.post('/facultySucessfullUpdatedStudent',urlencodedParser,function(req,res){

  res.redirect('/faculty1/?empid='+req.body.empid);
})

app.get('/faculty0',function(req,res){
  res.render('faculty0',{ emp : req.query.empid});
});
app.post('/faculty0',urlencodedParser,function(req,res){
  console.log(req.body.empid);
  res.redirect('/faculty01/?sem='+req.body.sem+'&id='+req.body.empid)
});
app.get('/faculty01',function(req,res){
  console.log(req.query.id);
  res.render('faculty01', {emp : req.query.id, sem : req.query.sem});
})
app.post('/faculty01', urlencodedParser, function(req,res){
  console.log(req.body);
  var temp= Array();
  temp = (req.body.sem).split(",");
  console.log(temp);
  Faculty.findOneAndUpdate({ empid : req.body.id}, { sem : temp, sub : [req.body.sem6sub, req.body.sem7sub] }).then(function(){
    console.log('asas');
  })
  /*var newFacultydata = facultyData(req.body).save(function(err,data){
    if(err) throw err;
    var temp = req.body.id;
    console.log(temp);
    res.render('facultyRegisterSuccess')
  //res.redirect('/faculty1/?empid='+temp+'&sem6sub='+req.body.sem6sub+'&sem7sub='+req.body.sem7sub+'&sem='+req.body.sem);
});*/
res.render('facultyRegisterSuccess');
})

//for handling requests

}
console.log('collection created');
